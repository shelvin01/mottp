#!/usr/bin/python

# ====================================================================================
#  Test TTP's EMOA on larger instances
# ====================================================================================

import os
import time

instances = [

"berlin52_n51_bounded-strongly-corr_01.ttp",
"berlin52_n255_bounded-strongly-corr_01.ttp",

]

# nb of iterations
n = 2


# best config
#        ls2p   lsbp   muip   mubp   popsize     i1     i2     i3
#        0.1479 0.295  0.5974 0.0708 400     0.8677 0.8593 0.6194
#        0.85   0.8516 0.6887 0.0026 360     0.7557 0.5632 0.8839

# test all instances
for instance in instances:
  for i in range(0,n):
    os.system("java -jar mottp-test-1.0.jar "+instance+"   ls2p=0.1479 lsbp=0.295 muip=0.5974 mubp=0.0708 popsize=400 bsrr=0.0 i1=0.8677 i2=0.8593 i3=0.6194")
    time.sleep(1)
