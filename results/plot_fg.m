%% plot data for SO/EMO
clc
%problemName='eil51_n150_bounded-strongly-corr_01';
%problemName='u159_n474_bounded-strongly-corr_01';
%problemName='a280_n837_bounded-strongly-corr_01';
%problemName='rat783_n2346_bounded-strongly-corr_01';
%problemName='eil51_n250_bounded-strongly-corr_01';
%problemName='berlin52_n255_bounded-strongly-corr_01';
problemName='kroA100_n495_bounded-strongly-corr_01';

%% EMO
pFront=load([problemName '.ttp.txt']);

f1=figure;
scatter(pFront(:,1),pFront(:,2), 25, pFront(:,3),'filled');
hold on;
c=colorbar;
ylabel(c,'TTP Score');
xlabel('Time');
ylabel('Profit');
[ttpScoreEMO,idx]=max(pFront(:,3));
txt1=['\leftarrow EMOA-TTP (' num2str(round( ttpScoreEMO )) ')'];
text(pFront(idx,1)+2,pFront(idx,2)+2,txt1);
ttl=title(problemName);
set(ttl,'Interpreter','none');

%% s5
referenceFileNameS5='res-s5-partial.csv';
[finalTimeS5,finalProfitS5,ttpScoreS5] = readBest(problemName,referenceFileNameS5);
scatter(finalTimeS5,finalProfitS5,25,ttpScoreS5,'filled','d');
txt2=['\leftarrow S5 (' num2str(round(ttpScoreS5)) ')'];
text(finalTimeS5+2,finalProfitS5-2,txt2);

%% MA2B
referenceFileNameMA2B='res-ma2b-C.csv';
[finalTimeMA2B,finalProfitMA2B,ttpScoreMA2B] = readBest(problemName,referenceFileNameMA2B);
scatter(finalTimeMA2B,finalProfitMA2B,25,ttpScoreMA2B,'filled','s');
txt2MA2B = ['\leftarrow MA2B (' num2str(round(ttpScoreMA2B)) ')'];
text(finalTimeMA2B+2,finalProfitMA2B+2,txt2MA2B);

%% MMA
referenceFileNameMA2B='res-MMAS-optlog.csv';
[finalTimeMMAS,finalProfitMMAS,ttpScoreMMAS] = readBest(problemName,referenceFileNameMA2B);
scatter(finalTimeMMAS,finalProfitMMAS,25,ttpScoreMMAS,'filled','s');
txt2MMAS = ['\leftarrow MMAS (' num2str(round(ttpScoreMMAS)) ')'];
text(finalTimeMMAS+950,finalProfitMMAS-600,txt2MMAS);
%% EMO then DP
%pFront=load(['DP-front.txt']);
%scatter(pFront(:,1),-pFront(:,2),25,pFront(:,3),'filled', '^');
%[ttpScoreEMO,idx]=max(pFront(:,3));
%txt1=['\leftarrow DP (' num2str(round( ttpScoreEMO )) ')'];
%text(pFront(idx,1)+2,-pFront(idx,2)-2,txt1);

%set(gca,'fontsize',8);
%% save figure in PNG/EPS
saveas(f1,[problemName,'.pdf']);
