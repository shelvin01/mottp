function plot3d()
    problemName='eil51_n150_bounded-strongly-corr_01';
    pFront=load([problemName,'.ttp.txt']);
    f1=figure;
    plot3(pFront(:,1),pFront(:,2),pFront(:,3),'k.','markers',12);
    xlabel('Profit');
    ylabel('Time');
    zlabel('TTP Score');
    ttl=title(problemName);
    set(ttl,'Interpreter','none');
    [~,idx]=min(pFront(:,3));
    txt1='\leftarrow Best MO solution';
    mo_ttpScore=pFront(idx,3);
    hold on;
    plot3(pFront(idx,1),pFront(idx,2),pFront(idx,3),'r+','markers',12);
    grid on;
    box on;
       
    referenceFileNameS5='s5-partial.csv';
    [finalTimeS5,finalProfitS5,ttpScoreS5]=readReferenceData(problemName,referenceFileNameS5);
    plot3(finalProfitS5,finalTimeS5,-ttpScoreS5,'g+','markers',12);
    referenceFileNameMA2B='ma2b-2016-10-03.csv';
    [finalTimeMA2B,finalProfitMA2B,ttpScoreMA2B]=readReferenceData(problemName,referenceFileNameMA2B);
    plot3(finalProfitMA2B,finalTimeMA2B,-ttpScoreMA2B,'b+','markers',12);
    legend('MO Solutions',['Best MO Solution: ',num2str(round(-1*mo_ttpScore))],['S5 Solution: ',num2str(round(ttpScoreS5))],...
        ['MA2B Solution: ',num2str(round(ttpScoreMA2B))]);
end