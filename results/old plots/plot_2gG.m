%% plot data for SO
clc
problemName='eil51_n150_bounded-strongly-corr_01';

pFront=load([problemName,'-gG.ttp.txt']);
f1=figure;
scatter(pFront(:,1),pFront(:,2),25,pFront(:,3),'filled');
hold on;
c=colorbar;
ylabel(c,'TTP Score');
xlabel('Profit');
ylabel('TTP score');
[ttpScoreEMO,idx]=max(pFront(:,3));
txt1=['\leftarrow EMO (' num2str(round( ttpScoreEMO )) ')'];
text(pFront(idx,1)+2,pFront(idx,2)-2,txt1);
ttl=title(problemName);
set(ttl,'Interpreter','none');

%% s5
referenceFileNameS5='s5-partial.csv';
[finalTimeS5,finalProfitS5,ttpScoreS5]=readReferenceData(problemName,referenceFileNameS5);
scatter(finalProfitS5,-ttpScoreS5,25,-ttpScoreS5,'filled','d');
txt2=['\leftarrow S5 (' num2str(round(ttpScoreS5)) ')'];
text(finalProfitS5+2,-ttpScoreS5-2,txt2);

%% MA2B
referenceFileNameMA2B='ma2b-2016-10-03.csv';
[finalTimeMA2B,finalProfitMA2B,ttpScoreMA2B]=readReferenceData(problemName,referenceFileNameMA2B);
scatter(finalProfitMA2B,-ttpScoreMA2B,25,-ttpScoreMA2B,'filled','s');
txt2MA2B = ['\leftarrow MA2B (' num2str(round(ttpScoreMA2B)) ')'];
text(finalProfitMA2B+2,-ttpScoreMA2B-2,txt2MA2B);

%% save figure in PNG
saveas(f1,[problemName,'-gG.png']);
