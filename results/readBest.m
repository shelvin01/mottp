function [finalTime,finalProfit,ttpScore]=readBest(problemName,referenceFileName)
    fid = fopen(referenceFileName);
    tline = fgets(fid);
    allRes = zeros(30,3);
    k = 1;
    bestSF = -Inf;
    bestK = k;
    while ischar(tline)

        C = strsplit(tline,' ');

        if strcmp( C{1},[problemName,'.ttp:'] )==1
            finalTime=str2num(C{6});
            finalProfit=-1*str2num(C{4});
            ttpScore=str2num(C{7});

            if ttpScore > bestSF
                bestSF = ttpScore;
                bestK = k;
            end

            allRes(k,:) = [finalTime finalProfit ttpScore];
            k = k+1;
        end
        tline = fgets(fid);
    end
    arrLen = k-1;
    fclose(fid);
%    bestK
    finalTime = allRes(bestK,1);
    finalProfit = allRes(bestK,2);
    ttpScore = allRes(bestK,3);

%    A=allRes(:,3)
%    B=A(1:arrLen,:)
%    B=sort(B)

end
