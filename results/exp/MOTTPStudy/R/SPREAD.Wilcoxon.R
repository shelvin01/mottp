write("", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex",append=FALSE)
resultDirectory<-"C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/data"
latexHeader <- function() {
  write("\\documentclass{article}", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)
  write("\\title{StandardStudy}", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)
  write("\\usepackage{amssymb}", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)
  write("\\author{A.J.Nebro}", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)
  write("\\begin{document}", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)
  write("\\maketitle", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)
  write("\\section{Tables}", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)
  write("\\", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)
}

latexTableHeader <- function(problem, tabularString, latexTableFirstLine) {
  write("\\begin{table}", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)
  write("\\caption{", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)
  write(problem, "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)
  write(".SPREAD.}", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)

  write("\\label{Table:", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)
  write(problem, "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)
  write(".SPREAD.}", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)

  write("\\centering", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)
  write("\\begin{scriptsize}", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)
  write("\\begin{tabular}{", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)
  write(tabularString, "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)
  write("}", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)
  write(latexTableFirstLine, "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)
  write("\\hline ", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)
}

printTableLine <- function(indicator, algorithm1, algorithm2, i, j, problem) { 
  file1<-paste(resultDirectory, algorithm1, sep="/")
  file1<-paste(file1, problem, sep="/")
  file1<-paste(file1, indicator, sep="/")
  data1<-scan(file1)
  file2<-paste(resultDirectory, algorithm2, sep="/")
  file2<-paste(file2, problem, sep="/")
  file2<-paste(file2, indicator, sep="/")
  data2<-scan(file2)
  if (i == j) {
    write("-- ", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)
  }
  else if (i < j) {
    if (is.finite(wilcox.test(data1, data2)$p.value) & wilcox.test(data1, data2)$p.value <= 0.05) {
      if (median(data1) <= median(data2)) {
        write("$\\blacktriangle$", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)
      }
      else {
        write("$\\triangledown$", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE) 
      }
    }
    else {
      write("--", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE) 
    }
  }
  else {
    write(" ", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)
  }
}

latexTableTail <- function() { 
  write("\\hline", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)
  write("\\end{tabular}", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)
  write("\\end{scriptsize}", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)
  write("\\end{table}", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)
}

latexTail <- function() { 
  write("\\end{document}", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)
}

### START OF SCRIPT 
# Constants
problemList <-c("MOTTP") 
algorithmList <-c("NSGAII") 
tabularString <-c("l") 
latexTableFirstLine <-c("\\hline \\\\ ") 
indicator<-"SPREAD"

 # Step 1.  Writes the latex header
latexHeader()
tabularString <-c("| l | ") 

latexTableFirstLine <-c("\\hline \\multicolumn{1}{|c|}{} \\\\") 

# Step 3. Problem loop 
latexTableHeader("MOTTP ", tabularString, latexTableFirstLine)

indx = 0
for (i in algorithmList) {
  if (i != "NSGAII") {
    write(i , "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)
    write(" & ", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)

    jndx = 0
    for (j in algorithmList) {
      for (problem in problemList) {
        if (jndx != 0) {
          if (i != j) {
            printTableLine(indicator, i, j, indx, jndx, problem)
          }
          else {
            write("  ", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)
          } 
          if (problem == "MOTTP") {
            if (j == "NSGAII") {
              write(" \\\\ ", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)
            } 
            else {
              write(" & ", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)
            }
          }
     else {
    write("&", "C:\Users\shelvin\Documents\mottp/results/exp//MOTTPStudy/R/SPREAD.Wilcoxon.tex", append=TRUE)
     }
        }
      }
      jndx = jndx + 1
    }
    indx = indx + 1
  }
} # for algorithm

  latexTableTail()

#Step 3. Writes the end of latex file 
latexTail()

