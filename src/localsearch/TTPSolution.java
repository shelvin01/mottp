package localsearch;

import core.MultiObjectiveTTP;
import org.uma.jmetal.solution.Solution;
import org.uma.jmetal.solution.impl.AbstractGenericSolution;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

/**
 * a TTP solution
 * tour: cities references, starts from 1
 * picking plan: '0' if the item is not picked,
 * 'city reference (i)' if picked (from city i)
 *
 * @author kyu
 */
public class TTPSolution
    extends AbstractGenericSolution<Object, MultiObjectiveTTP>
    implements Solution<Object> {

    public long fp;
    public double ft;
    public double ob;
    public long wend;
    // time accumulator
    public double[] timeAcc;
    // time record
    public double[] timeRec;
    // weight accumulator
    public long[] weightAcc;
    // weight record at each iteration
    public long[] weightRec;
    // tour mapper
    public int[] mapCI;
    private int[] tour;
    private int[] pickingPlan;

    /**
     * Constructor
     *
     * @param problem
     */
    public TTPSolution(MultiObjectiveTTP problem) {
        super(problem);
    }

    public TTPSolution(MultiObjectiveTTP problem, String filePath) {
        super(problem);

        File solFile = new File(filePath);
        BufferedReader br = null;

        int nbCities = 0, nbItems = 0;

        try {
            br = new BufferedReader(new FileReader(solFile));
            String line;

            // scan tour
            while ((line = br.readLine()) != null) {

                // number of cities
                if (line.startsWith("DIMENSION")) {
                    line = line.substring(line.indexOf(":")+1);
                    line = line.replaceAll("\\s+","");
                    nbCities = Integer.parseInt(line);
                }

                // number of items
                if (line.startsWith("NUMBER OF ITEMS")) {
                    line = line.substring(line.indexOf(":")+1);
                    line = line.replaceAll("\\s+","");
                    nbItems = Integer.parseInt(line);
                }

                if (line.startsWith("TOUR_SECTION")) {
                    this.tour = new int[nbCities];
                    for (int j=0; j<nbCities; j++) {
                        line = br.readLine();
                        tour[j] = Integer.parseInt(line);
                    }
                }

                if (line.startsWith("PP_SECTION")) {
                    this.pickingPlan = new int[nbItems];
                    for (int j=0; j<nbItems; j++) {
                        line = br.readLine();
                        pickingPlan[j] = Integer.parseInt(line);
                    }
                }
            } // end while

            br.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        this.initSolution(tour, pickingPlan);
    }


    public void initSolution(int[] tour, int[] pickingPlan) {
        this.tour = tour;
        this.pickingPlan = pickingPlan;

        // records
        this.timeAcc = new double[this.tour.length];
        this.timeRec = new double[this.tour.length];
        this.weightAcc = new long[this.tour.length];
        this.weightRec = new long[this.tour.length];
        this.mapCI = new int[this.tour.length];
    }

    @Override
    public String toString() {
        // the tour
        String s = "tsp tour    : (";
        for (int i = 0; i < tour.length; i++) {
            s += tour[i] + " ";
        }
        s = s.substring(0, s.length() - 1) + ")\n";

        // the picking plan
        s += "picking plan: (";
        for (int i = 0; i < pickingPlan.length; i++) {
            int pp = pickingPlan[i];
            s += pp + " ";
        }
        s = s.substring(0, s.length() - 1) + ")";

        return s;
    }

    @Override
    public TTPSolution clone() {
        TTPSolution copy = new TTPSolution(problem);

        copy.tour = Arrays.copyOf(this.tour, this.tour.length);
        copy.pickingPlan = Arrays.copyOf(this.pickingPlan, this.pickingPlan.length);
        copy.fp = this.fp;
        copy.ft = this.ft;
        copy.ob = this.ob;
        copy.wend = this.wend;
        copy.timeAcc = Arrays.copyOf(this.timeAcc, this.timeAcc.length);
        copy.timeRec = Arrays.copyOf(this.timeRec, this.timeRec.length);
        copy.weightAcc = Arrays.copyOf(this.weightAcc, this.weightAcc.length);
        copy.weightRec = Arrays.copyOf(this.weightRec, this.weightRec.length);
        copy.mapCI = Arrays.copyOf(this.mapCI, this.mapCI.length);

        return copy;
    }

    @Override
    public boolean equals(Object o2) {

        TTPSolution s2 = (TTPSolution) o2;

        for (int i = 0; i < this.tour.length; i++) {
            if (this.tour[i] != s2.tour[i]) return false;
        }
        for (int i = 0; i < this.pickingPlan.length; i++) {
            if (this.pickingPlan[i] != s2.pickingPlan[i]) return false;
        }

        return true;
    }

    // getters
    public int[] getTour() {
        return tour;
    }

    // setters
    public void setTour(int[] tour) {
        this.tour = tour;
    }

    public int[] getPickingPlan() {
        return pickingPlan;
    }

    public void setPickingPlan(int[] pickingPlan) {
        this.pickingPlan = pickingPlan;
    }

    public String output() {
        String s =
            "DIMENSION : " + tour.length + "\n" +
                "NUMBER OF ITEMS : " + pickingPlan.length + "\n" +
                "\n";

        s += "TOUR_SECTION\n";
        for (int x : tour) {
            s += x + "\n";
        }
        s += "\n";

        s += "PP_SECTION\n";
        for (int x : pickingPlan) {
            s += x + "\n";
        }

        s += "EOF";

        return s;
    }


    @Override
    public String getVariableValueString(int index) {
        return getVariableValue(index).toString();
    }

    @Override
    public Solution<Object> copy() {
        return null;
    }

}
