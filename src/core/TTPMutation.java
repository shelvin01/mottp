package core;

import localsearch.LocalSearch;
import localsearch.TTPSolution;
import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.util.JMetalException;
import org.uma.jmetal.util.pseudorandom.JMetalRandom;
import utils.Deb;

import java.util.BitSet;

@SuppressWarnings("serial")
public class TTPMutation implements MutationOperator<PermBinSolution> {

    // problem
    private MultiObjectiveTTP ttp;

    // local search
    private LocalSearch ls;

    // probabilities to apply an operator
    private double ls2optProb = 1.0;
    private double lsBitFlipProb = 1.0;
    private double muInsertProb = 1.0;
    private double muBitflipProb = 1.0;

    // rand gen
    private JMetalRandom randomGenerator;

    /**
     * constructor
     */
    public TTPMutation() {
        this.randomGenerator = JMetalRandom.getInstance();
    }

    /**
     * constructor 2
     */
    public TTPMutation(MultiObjectiveTTP ttp) {
        this();

        // set problem instance
        this.ttp = ttp;

        // use local search
        this.ls = new LocalSearch(ttp);
    }

    /**
     * execute() method
     */
    @Override
    public PermBinSolution execute(PermBinSolution solution) {
        if (null == solution) {
            throw new JMetalException("Null parameter");
        }

//        this.ls.maxIterKRP = 50;
//        this.ls.maxIterTSKP = 50;

        if (Math.random() < muBitflipProb) {
            muBitFlip(solution);
//            Deb.echo("EXE: mu bitflip");
        }

        if (Math.random() < muInsertProb) {
            muInsertion(solution);
//            Deb.echo("EXE: mu insert");
        }

        if (Math.random() < ls2optProb) {
            ls2opt(solution);
//            Deb.echo("EXE: ls 2opt");
        }

        if (Math.random() < lsBitFlipProb) {
            lsBitFlip(solution);
//            Deb.echo("EXE: ls bitflip");
        }

//        Deb.echo("EXE DONE");

        return solution;
    }

    // operator: flip some bits
    public void muBitFlip(PermBinSolution solution) {

        int bitsetIdx = solution.getNumberOfVariables() - 1;

        BitSet bitset = (BitSet) solution.getVariableValue(bitsetIdx);

        // percent of the number of items
        double probability = 10.0/solution.getNbItems();
        long newWend;

        for (int j = 0; j < solution.getNbItems(); j++) {
            if (Math.random() < probability) {
                if (bitset.get(j)) { // packed
                    solution.setWend(solution.getWend() + solution.getItemWeight(j));
                    bitset.flip(j);
                }
                else { // not packed
                    newWend = solution.getWend() - solution.getItemWeight(j);
                    if (newWend > 0) {
                        solution.setWend(newWend);
                        bitset.flip(j);
                    }
                }

            }
        }

        solution.setVariableValue(bitsetIdx, bitset);
    }

    // operator: change position of a city
    public void muInsertion(PermBinSolution solution) {
        int loc1 = randomGenerator.nextInt(1, solution.getNumberOfVariables() - 3);
        int loc2 = loc1;
        while (loc2 == loc1) {
            loc2 = randomGenerator.nextInt(1, solution.getNumberOfVariables() - 3);
        }
        int[] temp = new int[solution.getNbCities()];
        int tmpLoc;
        if (loc1 > loc2) {
            tmpLoc = loc1;
            loc1 = loc2;
            loc2 = tmpLoc;
        }
        int index = 0;
        for (int i = 0; i <= loc2; i++) {
            if (i != loc1) {
                temp[index] = (int) solution.getVariableValue(i);
                index++;
            }
        }
        temp[loc2] = (int) solution.getVariableValue(loc1);
        for (int i = (loc2 + 1); i < solution.getNbCities(); i++) {
            temp[i] = (int) solution.getVariableValue(i);
        }
        for (int i = 0; i < solution.getNbCities(); i++) {
            solution.setVariableValue(i, temp[i]);
        }

    }

    // local search: 2-opt
    public void ls2opt(PermBinSolution solution) {
        int[] tour = ttp.getTour(solution);
        int[] pp = ttp.getPickingPlan(solution);
        TTPSolution s0 = new TTPSolution(ttp);
        s0.initSolution(tour, pp);

        // reduce LS time
//        ls.maxIterTSKP = 50;
//        ls.maxIterKRP = 50;
        ls.firstfit();

        // apply LS
//        s0 = ls.slow2opt(s0);
        s0 = ls.fast2opt(s0);
        int[] ftour = s0.getTour();

        for (int i = 0; i < ttp.getNbCities(); i++) {
            solution.setVariableValue(i, ftour[i]);
        }

        // set solution params
        solution.setOb(s0.ob);
        solution.setWend(s0.wend);
    }

    // local search: bit flip
    public void lsBitFlip(PermBinSolution solution) {

        int bitsetIdx = solution.getNumberOfVariables() - 1;

        int[] tour = ttp.getTour(solution);
        int[] pp = ttp.getPickingPlan(solution);
        TTPSolution s0 = new TTPSolution(ttp);
        s0.initSolution(tour, pp);

        // reduce LS time
//        ls.maxIterTSKP = 50;
//        ls.maxIterKRP = 50;
        ls.firstfit();

        // apply LS
        s0 = ls.lsBitFlip(s0);
        int[] fpp = s0.getPickingPlan();

        // convert to bitset
        BitSet bitset = (BitSet) solution.getVariableValue(bitsetIdx);
        for (int j = 0; j < solution.getNbItems(); j++) {
            bitset.set(j, fpp[j]!=0);
        }
        solution.setVariableValue(bitsetIdx, bitset);

        // set solution params
        solution.setOb(s0.ob);
        solution.setWend(s0.wend);
    }





    // getters
    public double getLs2optProb() {
        return ls2optProb;
    }

    public double getLsBitFlipProb() {
        return lsBitFlipProb;
    }

    public double getMuBitflipProb() {
        return muBitflipProb;
    }

    public double getMuInsertProb() {
        return muInsertProb;
    }

    // setters
    public void setLs2optProb(double ls2optProb) {
        this.ls2optProb = ls2optProb;
    }

    public void setLsBitFlipProb(double lsBitFlipProb) {
        this.lsBitFlipProb = lsBitFlipProb;
    }

    public void setMuInsertProb(double muInsertProb) {
        this.muInsertProb = muInsertProb;
    }

    public void setMuBitflipProb(double muBitflipProb) {
        this.muBitflipProb = muBitflipProb;
    }

}
