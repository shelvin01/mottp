package core;

import org.uma.jmetal.solution.Solution;

public interface PermBinSolution extends Solution<Object> {
    public int getNbCities();

    public int getNbItems();

    public long getWend();

    public void setWend(long wend);

    public int[] getAllWeights();

    public int getItemWeight(int index);

    public int totalWeightPicked();

    public double getOb();

    public void setOb(double score);
}
