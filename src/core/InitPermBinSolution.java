package core;

import approx.Optimisation;
import approx.TTPInstance;
import localsearch.LocalSearch;
import localsearch.TTPSolution;
import org.uma.jmetal.solution.impl.AbstractGenericSolution;
import utils.ConfigHelper;
import utils.Deb;
import utils.RandGen;

import java.io.*;
import java.util.*;

/**
 * Description:
 * - this solution contains an array of permutation values + a binary string
 * - getNumberOfVariables() returns the number of permutation vars + 1 (the string)
 * - the bitset is the last variable
 */
public class InitPermBinSolution
    extends AbstractGenericSolution<Object, MultiObjectiveTTP>
    implements PermBinSolution {

    private int nbCities;
    private int nbItems;
    private long wend;
    private double ob;
    private int[] itemWeights;
    private int algoCode;

    /**
     * Constructor
     */
    public InitPermBinSolution(MultiObjectiveTTP ttp) {
        super(ttp);

        nbCities = ttp.getNbPermVars();
        nbItems = ttp.getNbItems();
        wend = ttp.getKnapsackCapacity();
        itemWeights = ttp.getAllWeights();
        ob = 0.0;

//        execute(ttp);

    }

    public void execute(MultiObjectiveTTP ttp) {
//        Deb.echo(">>>>>>>>"+algoCode);
//        initPermutation();
//        initBitSet();

        BitSet bitset;

        switch (algoCode) {

            // random PP
            case 1:
                initPermutation();

                // start with empty bit-set
                setVariableValue(getNbCities(), new BitSet(this.getNbItems()));
                // apply greedy pp
                bitset = bitSetRand();
                setVariableValue(getNbCities(), bitset);
            break;

            // greedy
            case 2:
                initPermutation();

                // start with empty bit-set
                setVariableValue(getNbCities(), new BitSet(this.getNbItems()));
                // apply greedy pp
                bitset = bitSetGreedy();
                setVariableValue(getNbCities(), bitset);
                break;

            // packIterative
            default:
                initPackIterative();
            break;
        }

        ttp.evaluate(this);

//        TTPMutation mu = new TTPMutation(ttp);
//        // mutate to diversify
//        mu.muBitFlip(this);
//        // improve tour
//        mu.ls2opt(this);
//        // improve pick plan
//        mu.lsBitFlip(this);
//        Deb.echo(">> INIT DONE");
//        ttp.evaluate(this);

        initializeObjectiveValues();

    }
    /**
     * Copy constructor
     */
    public InitPermBinSolution(InitPermBinSolution solution) {
        super(solution.problem);
        for (int i = 0; i < problem.getNumberOfObjectives(); i++) {
            setObjective(i, solution.getObjective(i));
        }
        this.nbItems = solution.getNbItems();
        this.nbCities = solution.getNbCities();
        this.wend = solution.getWend();
        this.itemWeights = solution.getAllWeights();
        this.ob = solution.getOb();
        copyPermutationVariables(solution);
        copyBitSet(solution);
        this.attributes = new HashMap<Object, Object>(solution.attributes);
    }



    ///////////////////////////
    // TOUR                  //
    ///////////////////////////

    /**
     * use a linkern tour
     */
    private void initPermutation() {

        // get linkern tour (depending on OS)
        int[] lkt;
        if (System.getProperty("os.name").contains("Linux")) {
            lkt = linkernExecute();
        }
        else {
            lkt = linkernFile();
        }

        setVariableValue(0, 1);
        for (int i = 1; i < this.getNbCities() - 1; i++) {
            setVariableValue(i, lkt[i]);
        }
        setVariableValue(this.getNbCities() - 1, 1);
    }

    /**
     * linkern tour / execute concorde
     */
    public int[] linkernExecute() {

        int nbCities = problem.getNbPermVars();
        int[] tour = new int[nbCities];

        String fileName = problem.getName();
        String instFolder = problem.getName().replaceAll("_.+", "") + "-ttp/";

        String tourFileName = fileName + "-" + RandGen.randStr(10) + ".tour";
        try {
            // execute linkern program
            String filePath = ConfigHelper.getProperty("ttpdata") + instFolder + fileName;
            String[] cmd = {
                "./bins/linkern/linkern",
                "-o", tourFileName,
                filePath
            };
            Runtime runtime = Runtime.getRuntime();
            Process proc = runtime.exec(cmd);

            proc.waitFor();

            // read output tour
            File tourFile = new File(tourFileName);
            BufferedReader br = new BufferedReader(new FileReader(tourFile));
            String line;
            br.readLine(); // skip first line
            int i = 0;
            while ((line = br.readLine()) != null) {
                String[] parts = line.split("\\s+");
                tour[i++] = 1 + Integer.parseInt(parts[0]);
            }
            tourFile.delete();
            br.close();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return tour;
    }

    /**
     * linkern tour / uses hardcoded tours
     */
    public int[] linkernFile() {

        int nbCities = problem.getNbPermVars();
        int[] tour = new int[nbCities];

        String instFolder = problem.getName().replaceAll("_.+", "") + "-ttp/";
        String fileName = instFolder.replaceAll("-.+", "");
        String dirName = ConfigHelper.getProperty("lktours");
        fileName += ".linkern.tour";

        File file = new File(dirName + "" + fileName);

//        Deb.echo(dirName + " / " + fileName);

        BufferedReader br = null;

        try {
            br = new BufferedReader(new FileReader(file));
            String line;

            // scan tour
            while ((line = br.readLine()) != null) {

                if (line.startsWith("TOUR_SECTION")) {

                    for (int j=0; j<nbCities; j++) {
                        line = br.readLine();
                        tour[j] = Integer.parseInt(line);
                    }
                }
            } // end while

            br.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return tour;
    }



    ///////////////////////////
    // PICK PLAN             //
    ///////////////////////////

    /**
     * use a random bit string
     */
    private void initBitSet() {
        // start with empty bit-set
        setVariableValue(getNbCities(), new BitSet(this.getNbItems()));

        // apply greedy pp
        BitSet bitset = bitSetGreedy();
        setVariableValue(getNbCities(), bitset);
    }

    /**
     * create a random bit string
     */
    private BitSet bitSetRand() {
        int nbItems = getNbItems();

        BitSet bitSet = new BitSet(nbItems);
        double prob = 1.0 / 10;
        ArrayList<Integer> idx = new ArrayList<>();
        for (int i = 0; i < nbItems; i++) {
            idx.add(i);
        }
        Collections.shuffle(idx); //use this random order to do bitflip

        for (int i = 0; i < nbItems; i++) {
            if (randomGenerator.nextDouble() < prob) {
                if ((this.getWend() - this.getItemWeight(idx.get(i))) > 0) {
                    bitSet.set(idx.get(i), true);
                    setWend(this.getWend() - this.getItemWeight(idx.get(i)));
                } else {
                    bitSet.set(idx.get(i), false);
                }

            } else {
                bitSet.set(idx.get(i), false);
            }
        }
        return bitSet;
    }

    /**
     * create a random bit string
     */
    private BitSet bitSetEmpty() {
        int nbItems = getNbItems();

        BitSet bitSet = new BitSet(nbItems);

        for (int i = 0; i < nbItems; i++) {
            bitSet.set(i, false);
        }
        return bitSet;
    }

    /**
     * create a random bit string
     */
    private BitSet bitSetGreedy() {

        int[] tour = this.problem.getTour(this);
        int[] pp = this.problem.getPickingPlan(this);

        TTPSolution sol = new TTPSolution(problem);
        sol.initSolution(tour, pp);

        LocalSearch ls = new LocalSearch(problem);
        sol = ls.insertT2(sol);
        int[] newpp = sol.getPickingPlan();

        setWend(sol.wend);
        setOb(sol.ob);

//        Deb.echo(">>> "+sol.ob+" | "+sol.fp+" | "+sol.ft+" | "+wend);

        // convert to BitSet
        BitSet bitSet = new BitSet(pp.length);

        for (int i = 0; i < pp.length; i++) {
            bitSet.set(i, newpp[i]!=0);
        }

        return bitSet;
    }

    /**
     * pack iterative
     */
    private BitSet bitSetPackIterativeJar() {

        // get current solution
        int[] tour = this.problem.getTour(this);
        int[] pp = this.problem.getPickingPlan(this);
        TTPSolution sol = new TTPSolution(problem);
        sol.initSolution(tour, pp);
        String randStr = RandGen.randStr(5);
        String outFileName = problem.getName() + "-" + randStr + "-OUTPUT.txt";

        // feed the input solution to approx jar
        String fileName = problem.getName();
        String instFolder = problem.getName().replaceAll("_.+", "") + "-ttp/";
        String dataFolder = ConfigHelper.getProperty("ttpdata");

        try {
            // execute approx jar
            String[] cmd = {
                "java", "-jar",                     // java cmd
                "./bins/approx/ttpapprox-1.0.jar",  // jar file
                dataFolder+"/"+instFolder,          // folder name
                fileName,                           // instance file name
                "71",                               // algo code
                "10000",                            //
                "60000",                            // maximum runtime
                "XXX",                              // input solution
                "./bins/approx/"+outFileName,       // output solution
            };
            Runtime runtime = Runtime.getRuntime();
            Process proc = runtime.exec(cmd);
            proc.waitFor();

            // show output
//            BufferedReader br = new BufferedReader(new InputStreamReader(proc.getInputStream()));
//            String line;
//            while ((line = br.readLine()) != null) {
//                Deb.echo(">>"+line);
//            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // check if file exists
        File outFile = new File("./bins/approx/"+outFileName);
        while (!outFile.exists()) {
//            Deb.echo("/!\\ File does not exist");
        }
//        Deb.echo("File found !");
        // read output solution
        TTPSolution sf = new TTPSolution(problem, "./bins/approx/"+outFileName);
//        Deb.echo(sf.getPickingPlan());
        int[] newpp = sf.getPickingPlan();
        problem.evaluate(sf);
//        Deb.echo(sf.fp+"/"+sf.ft+"/"+sf.ob+"/"+sf.wend);
        setWend(sf.wend);
        setOb(sf.ob);

        // delete solution files
//        File inFile = new File("./bins/approx/" + inFileName);
//        inFile.delete();
//        File outFile = new File("./bins/approx/" + outFileName);
//        outFile.delete();


//        Deb.echo(">>> "+sol.ob+" | "+sol.fp+" | "+sol.ft+" | "+wend);

        // convert to BitSet
        BitSet bitSet = new BitSet(pp.length);

        for (int i = 0; i < pp.length; i++) {
            bitSet.set(i, newpp[i]!=0);
        }

        return bitSet;
    }


    /**
     * Markus' code for linkern + PackIterative
     */
    private void initPackIterative() {

        // feed the input solution to approx jar
        String fileName = problem.getName();
        String instFolder = problem.getName().replaceAll("_.+", "") + "-ttp/";
        String dataFolder = ConfigHelper.getProperty("ttpdata");

        // setup and run algorithm
        int maxRuntime = 100000; // in ms
        approx.TTPInstance instance = new approx.TTPInstance(new File(dataFolder+"/"+instFolder+""+fileName));
        approx.TTPSolution newSolution = new approx.TTPSolution(new int[instance.numberOfNodes+1], new int[instance.numberOfItems]);
        instance.evaluate(newSolution, false);
        // execute and get solution
        newSolution = Optimisation.HT(instance, maxRuntime, true);
        int[] newTour = newSolution.normalTour();
        int[] newPP = newSolution.normalPP(instance);
//        Deb.echo(newTour);
//        Deb.echo(newPP);

        // read output solution
        TTPSolution sf = new TTPSolution(problem);
        sf.initSolution(newTour, newPP);

        problem.evaluate(sf);
//        Deb.echo(sf.fp+"/"+sf.ft+"/"+sf.ob+"/"+sf.wend);
        setWend(sf.wend);
        setOb(sf.ob);

        // convert to BitSet
        BitSet bitSet = new BitSet(newPP.length);

        for (int i = 0; i < newPP.length; i++) {
            bitSet.set(i, newPP[i]!=0);
        }

        // init tour
        setVariableValue(0, 1);
        for (int i = 1; i < this.getNbCities() - 1; i++) {
            setVariableValue(i, newTour[i]);
        }
        setVariableValue(this.getNbCities() - 1, 1);

        // start with empty bit-set
        setVariableValue(getNbCities(), bitSet);
    }








    ///////////////////////////////
    // GETTERS/SETTERS/HELPERS   //
    ///////////////////////////////

    private void copyPermutationVariables(InitPermBinSolution solution) {
        for (int i = 0; i < solution.getNbCities(); i++) {
            setVariableValue(i, solution.getVariableValue(i));
        }
    }

    private void copyBitSet(InitPermBinSolution solution) {
        BitSet bitset = (BitSet) solution.getVariableValue(solution.getNumberOfVariables() - 1);
        BitSet newBit = new BitSet(nbItems);
        for (int i = 0; i < nbItems; i++) {
            if (bitset.get(i)) {
                newBit.set(i, true);
            } else {
                newBit.set(i, false);
            }
        }
        setVariableValue(solution.getNumberOfVariables() - 1, newBit);
    }


    @Override
    public int getNbCities() {
        return this.nbCities;
    }

    @Override
    public int getNbItems() {
        return nbItems;
    }

    @Override
    public long getWend() {
        return this.wend;
    }

    @Override
    public void setWend(long wend) {
        this.wend = wend;
    }

    @Override
    public int getItemWeight(int index) {
        return itemWeights[index];
    }

    @Override
    public int[] getAllWeights() {
        int[] allWeights = new int[itemWeights.length];
        for (int i = 0; i < itemWeights.length; i++) {
            allWeights[i] = itemWeights[i];
        }
        return allWeights;
    }

    @Override
    public InitPermBinSolution copy() {
        return new InitPermBinSolution(this);
    }

    @Override
    public String getVariableValueString(int index) {
        return getVariableValue(index).toString()+"///"+getOb()+"///";
    }

    @Override
    public int totalWeightPicked() {
        BitSet bitset = (BitSet) this.getVariableValue(this.getNumberOfVariables() - 1);
        int sum = 0;
        for (int i = 0; i < getNbItems(); i++) {
            if (bitset.get(i)) {
                sum = sum + getItemWeight(i);
            }
        }
        return sum;
    }

    public double getOb() {
        return this.ob;
    }

    public void setOb(double score) {
        this.ob = score;
    }


    public int getAlgoCode() {
        return algoCode;
    }

    public void setAlgoCode(int algoCode) {
        this.algoCode = algoCode;
    }
}
