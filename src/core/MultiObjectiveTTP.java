package core;

import localsearch.TTPSolution;
import org.uma.jmetal.problem.impl.AbstractGenericProblem;
import utils.CityCoordinates;
import utils.ConfigHelper;
import utils.Deb;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.BitSet;


@SuppressWarnings("serial")
public class MultiObjectiveTTP
    extends AbstractGenericProblem<PermBinSolution>
    implements PermBinProblem<PermBinSolution> {

    private String name;
    private String knapsackDataType;
    private int nbCities;
    private double rent;
    private int nbItems;
    private long capacity;
    private double minSpeed;
    private double maxSpeed;
    private String edgeWeightType;
    private CityCoordinates[] coordinates;
    private long[][] dist = null;
    private int[] availability;
    private int[] profits;
    private int[] weights;
    private ArrayList<Integer>[] clusters;

    private int initAlgoCode = 3;


    /**
     * Creates a new TSP problem instance
     */
    public MultiObjectiveTTP(String instancePath, String instanceName) throws IOException {
        readProblem(instancePath + instanceName);
        setNbPermVars(nbCities);
        setNbItems(nbItems);
        setNumberOfVariables(nbCities + 2);
        setNumberOfObjectives(2);
        setNumberOfConstraints(1);
        setName(instanceName);
    }

    @Override
    public PermBinSolution createSolution() {
        InitPermBinSolution init = new InitPermBinSolution(this);
        init.setAlgoCode(initAlgoCode);
        init.execute(this);

        return init;
    }


    /**
     * Evaluate() method
     */
    public void evaluate(PermBinSolution solution) {

        int[] x = getTour(solution);
        int[] z = getPickingPlan(solution);

        //long[][] D = getDist();
        double C = (maxSpeed - minSpeed) / capacity; // velocity const
        double velocity;

        long acc;       // iteration weight accumulator
        long wc = 0;    // current weight
        long fp = 0;    // final profit
        double ft = 0;  // tour time
        double ob;      // objective value


        // visit all cities
        for (int i = 0; i < this.nbCities; i++) {
            acc = 0;
            // check only items contained in current city
            for (int j : clusters[x[i] - 1]) {
                if (z[j] != 0) {
                    fp += profits[j];
                    acc += weights[j];
                }
            }

            wc += acc;
            velocity = maxSpeed - wc * C;
            int h = (i + 1) % nbCities;
            ft += distFor(x[i] - 1, x[h] - 1) / velocity;
        }

        ob = fp - (ft * rent);

        //s.wend = capacity-wc;
        solution.setOb(ob);

        solution.setObjective(0, ft);
        solution.setObjective(1, -fp);
    }

    /**
     * objective function
     * used in local searches
     *
     * @param s the TTP solution
     */
    public void evaluate(TTPSolution s) {

        int[] x = s.getTour();
        int[] z = s.getPickingPlan();

        //long[][] D = getDist();
        double C = (maxSpeed-minSpeed)/capacity; // velocity const
        double velocity;

        long acc;       // iteration weight accumulator
        long wc = 0;    // current weight
        long fp = 0;    // final profit
        double ft = 0;  // tour time
        double ob;      // objective value


        // visit all cities
        for (int i=0; i<this.nbCities; i++) {
            acc = 0;
            // check only items contained in current city
            for (int j : clusters[ x[i]-1 ]) {
                if (z[j]!=0) {
                    fp += profits[j];
                    acc += weights[j];
                }
            }

            wc += acc;
            velocity = maxSpeed - wc*C;

            int h = (i+1)%nbCities;
            ft += distFor(x[i]-1,x[h]-1) / velocity;

            // record important data for future use
            s.timeAcc[i] = ft;
            s.timeRec[i] = distFor(x[i]-1, x[h]-1) / velocity;
            s.weightAcc[i] = wc;
            s.weightRec[i] = acc;

            // map indices to their associated cities
            s.mapCI[x[i]-1] = i;
        }

        ob = fp - ft*rent;

        // solution properties
        s.fp = fp;
        s.ft = ft;
        s.wend = capacity-wc;
        s.ob = ob;
    }


    public int[] getTour(PermBinSolution solution) {
        int[] tour = new int[this.getNbCities()];
        for (int i = 0; i < tour.length; i++) {
            tour[i] = (int) solution.getVariableValue(i);
        }
        return tour;
    }

    public int[] getPickingPlan(PermBinSolution solution) {
        int[] pickingPlan = new int[solution.getNbItems()];
        BitSet bitset
            = (BitSet) solution.getVariableValue(solution.getNumberOfVariables() - 1);

        for (int i = 0; i < this.getNbItems(); i++) {
            if (bitset.get(i)) {
                pickingPlan[i] = 1;
            } else {
                pickingPlan[i] = 0;
            }
        }
        return pickingPlan;
    }

    private void readProblem(String instanceFile) throws IOException {
        String ttpData = ConfigHelper.getProperty("ttpdata");
        File ttpFile = new File(ttpData + instanceFile);
        BufferedReader br = null;

        try {
            br = new BufferedReader(new FileReader(ttpFile));
            String line;

            while ((line = br.readLine()) != null) {

                // instance name
                if (line.startsWith("PROBLEM NAME")) {
                    line = line.substring(line.indexOf(":") + 1);
                    line = line.replaceAll("\\s+", "");
                    this.name = line;
                }

                // KP data type
                if (line.startsWith("KNAPSACK DATA TYPE")) {
                    line = line.substring(line.indexOf(":") + 1);
                    line = line.replaceAll("\\s+", "");
                    this.knapsackDataType = line;
                }

                // number of cities
                if (line.startsWith("DIMENSION")) {
                    // if (line.startsWith("NUMBER OF NODES")) {
                    line = line.substring(line.indexOf(":") + 1);
                    line = line.replaceAll("\\s+", "");
                    this.nbCities = Integer.parseInt(line);
                }

                // number of items
                if (line.startsWith("NUMBER OF ITEMS")) {
                    line = line.substring(line.indexOf(":") + 1);
                    line = line.replaceAll("\\s+", "");
                    this.nbItems = Integer.parseInt(line);
                }

                // knapsack capacity
                if (line.startsWith("CAPACITY OF KNAPSACK")) {
                    line = line.substring(line.indexOf(":") + 1);
                    line = line.replaceAll("\\s+", "");
                    this.capacity = Long.parseLong(line);
                }

                // minimum velocity
                if (line.startsWith("MIN SPEED")) {
                    line = line.substring(line.indexOf(":") + 1);
                    line = line.replaceAll("\\s+", "");
                    this.minSpeed = Double.parseDouble(line);
                }

                // maximum velocity
                if (line.startsWith("MAX SPEED")) {
                    line = line.substring(line.indexOf(":") + 1);
                    line = line.replaceAll("\\s+", "");
                    this.maxSpeed = Double.parseDouble(line);
                }

                // rent
                if (line.startsWith("RENTING RATIO")) {
                    line = line.substring(line.indexOf(":") + 1);
                    line = line.replaceAll("\\s+", "");
                    this.rent = Double.parseDouble(line);
                }

                // edge weight
                if (line.startsWith("EDGE_WEIGHT_TYPE")) {
                    line = line.substring(line.indexOf(":") + 1);
                    line = line.replaceAll("\\s+", "");
                    this.edgeWeightType = line;
                }

                // nodes
                if (line.startsWith("NODE_COORD_SECTION")) {
                    // coordinates
                    this.coordinates = new CityCoordinates[this.nbCities];
                    for (int i = 0; i < this.nbCities; i++) {
                        line = br.readLine();
                        String[] parts = line.split("\\s+");
                        this.coordinates[i] = new CityCoordinates(Double.parseDouble(parts[1]), Double.parseDouble(parts[2]));
                    }

                    // distance matrix
                    if (nbCities < 10000) {
                        this.setDist(new long[this.nbCities][this.nbCities]);
                        for (int i = 0; i < nbCities; i++) {
                            for (int j = 0; j < nbCities; j++) {
//                                getDist()[i][j] = Math.round(this.coordinates[i].distanceEuclid(this.coordinates[j]));
                                getDist()[i][j] = (long)Math.ceil(this.coordinates[i].distanceEuclid(this.coordinates[j]));
                                //System.out.println(this.coord[i] + "&" + this.coord[j] + "->" + dist[i][j]);
                            }
                        }
                    }
                }

                // items
                if (line.startsWith("ITEMS SECTION")) {
                    this.profits = new int[this.nbItems];
                    this.weights = new int[this.nbItems];
                    this.availability = new int[this.nbItems];

                    for (int i = 0; i < this.nbItems; i++) {
                        line = br.readLine();
                        String[] splittedLine = line.split("\\s+");

                        this.profits[i] = Integer.parseInt(splittedLine[1]);
                        this.weights[i] = Integer.parseInt(splittedLine[2]);
                        this.availability[i] = Integer.parseInt(splittedLine[3]);
                    }
                }

            } // end while

            br.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        clusterItems();
    }

    public long[][] getDist() {
        return dist;
    }

    public void setDist(long[][] dist) {
        this.dist = dist;
    }

    // used to simulate the distance matrix
    public long distFor(int i, int j) {
        if (dist == null) {
            return (long)Math.ceil(this.coordinates[i].distanceEuclid(this.coordinates[j]));
        }
        return dist[i][j];
    }

    /**
     * organize items per city
     */
    private void clusterItems() {

        clusters = new ArrayList[nbCities];
        int i;

        // init cluster arrays
        for (i = 0; i < nbCities; i++) {
            clusters[i] = new ArrayList<>();
        }
        // fill clusters
        for (i = 0; i < nbItems; i++) {
            clusters[availability[i] - 1].add(i);
        }
    }



    public int getKnapsackCapacity() {
        return (int) capacity;
    }

    public int weightOf(int index) {
        return weights[index];
    }

    public int profitOf(int i) {
        return this.profits[i];
    }

    public int[] getAllWeights() {
        int[] allWeights = new int[weights.length];
        for (int i = 0; i < weights.length; i++) {
            allWeights[i] = weights[i];
        }
        return allWeights;
    }


    public int[] getAvailability() {
        return availability;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public double getMinSpeed() {
        return minSpeed;
    }

    public long getCapacity() {
        return capacity;
    }

    public double getRent() {
        return rent;
    }


    @Override
    public int getNbItems() {
        return nbItems;
    }

    @Override
    public int getNbPermVars() {
        return nbCities + 1;
    }

    public int getNbCities() {
        return nbCities;
    }

    protected void setNbPermVars(int nbCities) {
        this.nbCities = nbCities;
    }

    protected void setNbItems(int nbItems) {
        this.nbItems = nbItems;
    }

    public CityCoordinates[] getCoordinates() {
        return coordinates;
    }

    public int getInitAlgoCode() {
        return initAlgoCode;
    }

    public void setInitAlgoCode(int initAlgoCode) {
        this.initAlgoCode = initAlgoCode;
    }
}
