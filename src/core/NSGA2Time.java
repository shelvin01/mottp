package core;

import org.uma.jmetal.algorithm.multiobjective.nsgaii.NSGAII;
import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.operator.SelectionOperator;
import org.uma.jmetal.problem.ConstrainedProblem;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.solution.Solution;
import org.uma.jmetal.util.solutionattribute.Ranking;
import utils.Deb;
import utils.Quicksort;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * This class shows a version of NSGA-II having a stopping condition depending on run-time
 *
 * @author Antonio J. Nebro <antonio@lcc.uma.es>
 */
@SuppressWarnings("serial")
public class NSGA2Time<S extends PermBinSolution> extends NSGAII<S> {

    private long initComputingTime;

    private long thresholdComputingTime;

    private boolean stoppingCondition;

    private double removalRate = 0.0;

    private double initRandRate = .3;
    private double initGreedyRate = .3;
    private double initPackItRate = .4;

    /**
     * Constructor
     */
    public NSGA2Time(Problem<S> problem, int populationSize,
                     long maxComputingTime,
                     CrossoverOperator<S> crossoverOperator, MutationOperator<S> mutationOperator,
                     SelectionOperator<List<S>, S> selectionOperator) {
        super(problem, 0, populationSize, crossoverOperator, mutationOperator,
            selectionOperator, null);

        initComputingTime = System.currentTimeMillis();
        stoppingCondition = false;
        thresholdComputingTime = maxComputingTime;
    }

    @Override
    protected void initProgress() {
        evaluations = getMaxPopulationSize();
    }

    @Override
    protected void updateProgress() {
    }

    @Override
    protected List<S> evaluatePopulation(List<S> population) {
        int index = 0;

        while ((index < population.size()) && !stoppingCondition) {
            if (getProblem() instanceof ConstrainedProblem) {
                getProblem().evaluate(population.get(index));
                ((ConstrainedProblem<S>) getProblem()).evaluateConstraints(population.get(index));
            } else {
                getProblem().evaluate(population.get(index));
            }

            if ((System.currentTimeMillis() - initComputingTime) > thresholdComputingTime) {
                stoppingCondition = true;
            } else {
                evaluations++;
                index++;
            }
        }

        return population;
    }

    @Override
    protected boolean isStoppingConditionReached() {
        return stoppingCondition;
    }

    @Override
    public String getName() {
        return "NSGAII";
    }

    @Override
    public String getDescription() {
        return "NSGAII";
    }


    /**
     * customized selection strategy
     * uses TTP score to keep the best individuals
     */
    @Override
    public List<S> replacement(List<S> population, List<S> offspringPopulation) {

        // skip all this if replacement rate is null
        if (removalRate < 0.0001)
            return super.replacement(population,offspringPopulation);

        List<S> jointPopulation = new ArrayList<>();
        jointPopulation.addAll(population);
        jointPopulation.addAll(offspringPopulation);

        ArrayList<S> sortedPop = new ArrayList<S>(jointPopulation);

        // sort according to TTP score
        // decreasing order
        sortedPop.sort(new Comparator<S>() {
            @Override
            public int compare(S s1, S s2) {
                if (s1.getOb()<s2.getOb()) return 1;
                if (s1.getOb()>s2.getOb()) return -1;
                return 0;
            }
        });

        long N = sortedPop.size();
        long nbRemove = Math.round(removalRate * N);

        for (long i=0; i<nbRemove; i++) {
            sortedPop.remove(sortedPop.size() - 1);
        }

        Ranking<S> ranking = computeRanking(sortedPop);

        return crowdingDistanceSelection(ranking);
    }

    @Override
    public List<S> createInitialPopulation() {
        int N = getMaxPopulationSize();
        List<S> population = new ArrayList<>(N);
        long N1 = Math.round(N*initRandRate);
        long N2 = Math.round(N*initGreedyRate);
        long N3 = N-N1-N2;
//        Deb.echo(N+": "+N1+" | "+N2+" | "+N3);
        MultiObjectiveTTP ttp = (MultiObjectiveTTP) getProblem();

        // random init
        for (int i = 0; i < N1; i++) {
            ttp.setInitAlgoCode(1);
            S newIndividual = (S) ttp.createSolution();
            population.add(newIndividual);
        }

        // greedy init
        for (int i = 0; i < N2; i++) {
            ttp.setInitAlgoCode(2);
            S newIndividual = (S) ttp.createSolution();
//            S newIndividual = getProblem().createSolution();
            population.add(newIndividual);
        }

        // packIterative
        for (int i = 0; i < N3; i++) {
            ttp.setInitAlgoCode(3);
            S newIndividual = (S) ttp.createSolution();
//            S newIndividual = getProblem().createSolution();
            population.add(newIndividual);
        }

        return population;
    }




    public double getRemovalRate() {
        return removalRate;
    }

    public double getInitGreedyRate() {
        return initGreedyRate;
    }

    public double getInitRandRate() {
        return initRandRate;
    }

    public double getInitPackItRate() {
        return initPackItRate;
    }

    public void setRemovalRate(double removalRate) {
        this.removalRate = removalRate;
    }

    public void setInitGreedyRate(double initGreedyRate) {
        this.initGreedyRate = initGreedyRate;
    }

    public void setInitRandRate(double initRandRate) {
        this.initRandRate = initRandRate;
    }

    public void setInitPackItRate(double initPackItRate) {
        this.initPackItRate = initPackItRate;
    }

}
