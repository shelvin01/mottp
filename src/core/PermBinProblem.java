package core;

import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.solution.Solution;

public interface PermBinProblem<S extends Solution<Object>> extends Problem<S> {

    public int getNbPermVars();

    public int getNbItems();

    public int getKnapsackCapacity();

    public int weightOf(int index);

    public int[] getAllWeights();
}