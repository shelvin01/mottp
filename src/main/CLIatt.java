package main;

import core.MultiObjectiveTTP;
import core.NSGA2Time;
import core.PermBinSolution;
import core.TTPMutation;
import org.uma.jmetal.algorithm.Algorithm;
import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.operator.SelectionOperator;
import org.uma.jmetal.operator.impl.crossover.NullCrossover;
import org.uma.jmetal.operator.impl.selection.BinaryTournamentSelection;
import org.uma.jmetal.util.AlgorithmRunner;
import org.uma.jmetal.util.JMetalLogger;
import org.uma.jmetal.util.comparator.RankingAndCrowdingDistanceComparator;
import utils.Deb;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

/**
 * test one instance
 * use in attainment plots
 */
public class CLIatt {

    public static void main(String[] args) throws IOException {

        if (args.length < 1) // test it
            args = new String[]{
                "eil51_n150_bounded-strongly-corr_01.ttp",
                "20"
            };

        // irace optimized params / tiny config
        double ls2p  = 0.3742;
        double lsbp  = 0.8303;
        double muip  = 0.2129;
        double mubp  = 0.3735;
        int popsize  = 380;
        double init1 = 0.0355;
        double init2 = 0.1762;
        double init3 = 0.9121;


        // normalize initialization rates
        double sum = init1 + init2 + init3;
        init1 = init1/sum;
        init2 = init2/sum;
        init3 = init3/sum;

        // get instance name
        String instancePath = args[0].split("_", 2)[0] + "-ttp/";
        String instanceName = args[0];

        // get number of repetitions
        int nbRep = 10;
        if (args.length > 0)
            nbRep = Integer.parseInt(args[1]);

        Deb.echo(">> " + nbRep + " repetitions | "+instanceName);
        for (int k=0; k<nbRep; k++) {

            // Define problem
            MultiObjectiveTTP problem = new MultiObjectiveTTP(instancePath, instanceName);

            // Define crossover operator
            CrossoverOperator<PermBinSolution> crossover =
                new NullCrossover<PermBinSolution>();

            // Mutation operator
            TTPMutation mutation = new TTPMutation(problem);

            mutation.setLs2optProb(ls2p);
            mutation.setLsBitFlipProb(lsbp);
            mutation.setMuInsertProb(muip);
            mutation.setMuBitflipProb(mubp);

            // selection operator
            SelectionOperator<List<PermBinSolution>, PermBinSolution> selection =
                new BinaryTournamentSelection<PermBinSolution>(
                    new RankingAndCrowdingDistanceComparator<PermBinSolution>());

            // define and run algo
            NSGA2Time algorithm;

            int thresholdComputingTimeInMilliseconds = 600000;
            int populationSize = popsize;

            algorithm = new NSGA2Time<PermBinSolution>(
                problem,
                populationSize,
                thresholdComputingTimeInMilliseconds,
                crossover,
                mutation,
                selection);

            algorithm.setInitRandRate(init1);
            algorithm.setInitGreedyRate(init2);
            algorithm.setInitPackItRate(init3);

            AlgorithmRunner algorithmRunner = new AlgorithmRunner.Executor(algorithm)
                .execute();

            List<PermBinSolution> population = algorithm.getResult();
            long computingTime = algorithmRunner.getComputingTime();

            JMetalLogger.logger.info("Total execution time: " + computingTime + "ms");

            printFinalSolutionSet(population);
            writeToFile(population, instanceName);
            Deb.echo(">>> " + k + " is done");
        }
    }

    public static void printFinalSolutionSet(List<PermBinSolution> population) {

        Deb.echo("Pareto solutions");
        for(int i=0; i<population.size();i++){
            Deb.echo(population.get(i).getObjective(0)+"  "+population.get(i).getObjective(1));
        }

    }

    public static void writeToFile(List<PermBinSolution> population,String instanceName) throws IOException {

        //PrintWriter pw = new PrintWriter("/results/" + instanceName + "-att.txt");
        String filename = "./results/ATT-" + instanceName + ".txt";
        File file = new File(filename);

        // if file doesn't exists, then create it
        if (!file.exists()) {
            file.createNewFile();
        }

        // write objectives to file
        for(int i=0; i<population.size();i++) {
            Files.write(Paths.get(filename),
                (population.get(i).getObjective(0)+"  "+population.get(i).getObjective(1)+"\n").
                    getBytes(), StandardOpenOption.APPEND);
        }
        Files.write(Paths.get(filename),
            "\n".getBytes(), StandardOpenOption.APPEND);
    }

}
