package main;

import core.MultiObjectiveTTP;
import localsearch.TTPSolution;
import org.uma.jmetal.operator.impl.crossover.NullCrossover;
import utils.Deb;
import org.uma.jmetal.algorithm.Algorithm;
import org.uma.jmetal.util.comparator.RankingAndCrowdingDistanceComparator;
import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.operator.SelectionOperator;
import org.uma.jmetal.operator.impl.selection.BinaryTournamentSelection;
import org.uma.jmetal.util.AlgorithmRunner;
import org.uma.jmetal.util.JMetalLogger;
import core.*;
import java.io.*;
import java.io.IOException;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import static java.nio.file.StandardCopyOption.*;

/**
 * test one instance
 */
public class CLI2 {

    public static void main(String[] args) throws IOException {

        // instance name
        String instanceName = "eil51_n250_bounded-strongly-corr_01.ttp";
//        String instanceName = "berlin52_n255_bounded-strongly-corr_01.ttp";
//        String instanceName = "eil76_n375_bounded-strongly-corr_01.ttp";
//        String instanceName = "kroA100_n495_bounded-strongly-corr_01.ttp";
//        String instanceName = "u159_n790_bounded-strongly-corr_01.ttp";
//        String instanceName = "a280_n837_bounded-strongly-corr_01.ttp";

        // setup best parameters
        double ls2p = 0.7472;
        double lsbp = 0.2684;
        double muip = 0.4815;
        double mubp = 0.5224;
        int popsize = 340;
        double bsrr = 0.0;
        double init1 = 0.7957;
        double init2 = 0.9323;
        double init3 = 0.2627;

        // normalize initialization rates
        double sum = init1 + init2 + init3;
        init1 = init1/sum;
        init2 = init2/sum;
        init3 = init3/sum;

        // problem
        String instancePath = instanceName.split("_", 2)[0] + "-ttp/";
        MultiObjectiveTTP problem = new MultiObjectiveTTP(instancePath, instanceName);

        // crossover
        CrossoverOperator<PermBinSolution> crossover =
            new NullCrossover<>();

        // mutations
        TTPMutation mutation = new TTPMutation(problem);
        mutation.setLs2optProb(ls2p);
        mutation.setLsBitFlipProb(lsbp);
        mutation.setMuInsertProb(muip);
        mutation.setMuBitflipProb(mubp);

        // selection operator
        SelectionOperator<List<PermBinSolution>, PermBinSolution> selection =
            new BinaryTournamentSelection<>(
                new RankingAndCrowdingDistanceComparator<PermBinSolution>());

        // define and run algo
        int maxRuntimeMS = 600000;
        //double removalRate = .1;

        NSGA2Time algorithm = new NSGA2Time<>(
            problem,
            popsize,
            maxRuntimeMS,
            crossover,
            mutation,
            selection
        );
        // set other algo params
        algorithm.setRemovalRate(bsrr);
        algorithm.setInitRandRate(init1);
        algorithm.setInitGreedyRate(init2);
        algorithm.setInitPackItRate(init3);

        AlgorithmRunner algorithmRunner = new AlgorithmRunner.Executor(algorithm).execute();

        List<PermBinSolution> population = algorithm.getResult() ;
        long computingTime = algorithmRunner.getComputingTime() ;

        JMetalLogger.logger.info("Total execution time: " + computingTime + "ms");

        // print objectives
        printFinalSolutionSet(population);

        // save objectives
        writeToFile(population, instanceName);

        // save the tours (arc format)
//        saveTours(population, instanceName, problem);



        // debug negative TTP score
        for(int i=0; i<population.size();i++) {
            if (population.get(i).getOb() < 0) {
                String currentTime = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss").format(new Date());
                File solFile = new File("./results/B_" + instanceName + "-" + currentTime + ".txt");
                FileWriter bfw = new FileWriter(solFile.getAbsoluteFile());
                BufferedWriter bbw = new BufferedWriter(bfw);
                PermBinSolution sol = population.get(i);

                // convert to TTPSolution
                int[] tour = problem.getTour(sol);
                int[] pp = problem.getPickingPlan(sol);
                TTPSolution s2 = new TTPSolution(problem);
                s2.initSolution(tour, pp);

                bbw.write(s2.toString());
                bbw.close();
            }
        }
    }


    // print objectives
    public static void printFinalSolutionSet(List<PermBinSolution> population) {

        Deb.echo("Pareto solutions:");
        for(int i=0; i<population.size(); i++) {
            Deb.echo(population.get(i).getObjective(0) + "  " + population.get(i).getObjective(1));
        }
    }

    // save objectives
    public static void writeToFile(List<PermBinSolution> population, String instanceName) throws IOException {

        final String dir = System.getProperty("user.dir");
        String currentTime = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss").format(new Date());
        File file = new File(dir + "/results/log/" + instanceName + "-" + currentTime + ".txt");

        // if file doesn't exists, then create it
        try {
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);

            for(int i=0; i<population.size();i++) {
                bw.write(population.get(i).getObjective(0)+"  "+population.get(i).getObjective(1)+"  "+population.get(i).getOb() +" "+ population.get(i).getWend()+"  "+population.get(i).totalWeightPicked()+"\n");
            }
            bw.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        Files.copy(file.toPath(), new File(dir + "/results/" + instanceName + ".txt").toPath(), REPLACE_EXISTING);
    }

    // save tour to file (linkern format)
    public static void saveTours(List<PermBinSolution> population, String instName, MultiObjectiveTTP problem) throws FileNotFoundException {

        for (int i = 0; i < population.size(); i++) {
            PrintWriter pw = new PrintWriter("./results/solutions/"+instName+"-"+i+".tour");
            int[] tour = problem.getTour(population.get(i));
            pw.println( (tour.length-1) + " " + (tour.length-1) );
            for (int j = 0; j < tour.length-1; j++) {
                long dist = problem.distFor(tour[j] - 1, tour[j+1] - 1 );
                pw.println((tour[j] - 1) + " " + (tour[j+1] - 1) + " " + dist);
            }
            pw.close();
        }
    }

}
