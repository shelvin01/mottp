package main;

import core.MultiObjectiveTTP;
import core.NSGA2Time;
import core.PermBinSolution;
import core.TTPMutation;
import org.uma.jmetal.algorithm.Algorithm;
import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.operator.SelectionOperator;
import org.uma.jmetal.operator.impl.crossover.NullCrossover;
import org.uma.jmetal.operator.impl.selection.BinaryTournamentSelection;
import org.uma.jmetal.util.AlgorithmRunner;
import org.uma.jmetal.util.comparator.RankingAndCrowdingDistanceComparator;
import utils.Deb;

import java.io.IOException;
import java.util.List;

/**
 * code for irace
 * tuning of mutation probabilities
 * uses the TTP score as a tuning fitness
 */
public class CLIirace {

    public static void main(String[] args) throws IOException {

        if (args.length < 1) // test it
            args = new String[]{
                "eil51_n150_bounded-strongly-corr_01.ttp",
                "ls2p=0.7472", "lsbp=0.2684", "muip=0.4815", "mubp=0.5224",
                "popsize=340",
                "bsrr=.4194",
                "i1=0.7957", "i2=0.9323", "i3=0.2627"
            };

//        .ID.  ls2p    lsbp    muip    mubp    popsize  bsrr    i1      i2      i3
//        92    0.7472  0.2684  0.4815  0.5224  340      0.4194  0.7957  0.9323  0.2627

        // get instance name
        String instancePath = args[0].split("_", 2)[0] + "-ttp/";
        String instanceName = args[0];

        // get probabilities
        double ls2optProb = 0.0;
        double lsBitFlipProb = 0.0;
        double muInsertProb = 0.0;
        double muBitFipProb = 0.0;

        // get population size
        int popSize = 100;

        double init1 = 0.0;
        double init2 = 0.0;
        double init3 = 1.0;

        // bias selection rate
        double biasSelRate = 0.0;

        // get algo params
        for (int k=1; k<=9; k++) {

            if (args[k].split("=", 2)[0].contains("ls2p"))
                ls2optProb = Double.parseDouble(args[k].split("=", 2)[1]);

            if (args[k].split("=", 2)[0].contains("lsbp"))
                lsBitFlipProb= Double.parseDouble(args[k].split("=", 2)[1]);

            if (args[k].split("=", 2)[0].contains("muip"))
                muInsertProb = Double.parseDouble(args[k].split("=", 2)[1]);

            if (args[k].split("=", 2)[0].contains("mubp"))
                muBitFipProb = Double.parseDouble(args[k].split("=", 2)[1]);

            if (args[k].split("=", 2)[0].contains("popsize"))
                popSize = Integer.parseInt(args[k].split("=", 2)[1]);

            if (args[k].split("=", 2)[0].contains("bsrr"))
                biasSelRate = Double.parseDouble(args[k].split("=", 2)[1]);

            if (args[k].split("=", 2)[0].contains("i1"))
                init1 = Double.parseDouble(args[k].split("=", 2)[1]);
            if (args[k].split("=", 2)[0].contains("i2"))
                init2 = Double.parseDouble(args[k].split("=", 2)[1]);
            if (args[k].split("=", 2)[0].contains("i3"))
                init3 = Double.parseDouble(args[k].split("=", 2)[1]);
        }
        // normalize initialization rates
        double sum = init1 + init2 + init3;
        init1 = init1/sum;
        init2 = init2/sum;
        init3 = init3/sum;

//        Deb.echo(ls2optProb+"/"+lsBitFlipProb+"/"+muInsertProb+"/"+muBitFipProb+"/"+popSize+"/"+biasSelRate);
//        Deb.echo(sum+" :: "+(init1+init2+init3)+" :: "+init1+"/"+init2+"/"+init3);

        // problem
        MultiObjectiveTTP problem = new MultiObjectiveTTP(instancePath, instanceName);

        // crossover
        CrossoverOperator<PermBinSolution> crossover =
            new NullCrossover<>();

        // mutations
        TTPMutation mutation = new TTPMutation(problem);

        // mutation probabilities
        mutation.setLs2optProb(ls2optProb);
        mutation.setLsBitFlipProb(lsBitFlipProb);
        mutation.setMuInsertProb(muInsertProb);
        mutation.setMuBitflipProb(muBitFipProb);

        // selection operator
        SelectionOperator<List<PermBinSolution>, PermBinSolution> selection =
            new BinaryTournamentSelection<>(
                new RankingAndCrowdingDistanceComparator<PermBinSolution>());

        // define and run algo
        int maxRuntimeMS = 600000;
        NSGA2Time<PermBinSolution> algorithm = new NSGA2Time<>(
            problem,
            popSize,
            maxRuntimeMS,
            crossover,
            mutation,
            selection);

        // set other algo params
        algorithm.setRemovalRate(biasSelRate);
        algorithm.setInitRandRate(init1);
        algorithm.setInitGreedyRate(init2);
        algorithm.setInitPackItRate(init3);

        AlgorithmRunner algorithmRunner = new AlgorithmRunner.Executor(algorithm).execute();

        List<PermBinSolution> population = algorithm.getResult();
        long computingTime = algorithmRunner.getComputingTime();

        // print best TTP score
        printBestTTPScore(population);

    }


    // print best found TTP score
    public static void printBestTTPScore(List<PermBinSolution> population) {

        double bestSF = Double.MIN_VALUE;
        for (int i = 0; i < population.size(); i++) {
            double ob = population.get(i).getOb();
            if (ob > bestSF)
                bestSF = ob;
        }

        Deb.echo( (-1*Math.round(bestSF)) );
    }

}
