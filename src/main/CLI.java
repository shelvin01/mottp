package main;

import core.*;
import org.uma.jmetal.algorithm.Algorithm;
import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.operator.SelectionOperator;
import org.uma.jmetal.operator.impl.crossover.NullCrossover;
import org.uma.jmetal.operator.impl.selection.BinaryTournamentSelection;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.qualityindicator.impl.Spread;
import org.uma.jmetal.qualityindicator.impl.hypervolume.PISAHypervolume;
import org.uma.jmetal.util.comparator.RankingAndCrowdingDistanceComparator;
import org.uma.jmetal.util.experiment.Experiment;
import org.uma.jmetal.util.experiment.ExperimentBuilder;
import org.uma.jmetal.util.experiment.component.ExecuteAlgorithms;
import org.uma.jmetal.util.experiment.util.TaggedAlgorithm;
import utils.Deb;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by yafrani on 8/10/16.
 */
public class CLI {

    public static void main(String[] args) throws IOException {
        String experimentBaseDirectory = System.getProperty("user.dir") + "/results/exp/";
        String instancePath = "eil51-ttp/";
        String[] instanceName = {
            "eil51_n150_bounded-strongly-corr_01.ttp",
            "eil51_n150_uncorr_01.ttp"
        };

        // Define problem
        PermBinProblem<PermBinSolution> problem;
        List<Problem<PermBinSolution>> problemList = new ArrayList<>();
        for (int j = 0; j < instanceName.length; j++) {
            problemList.add(new MultiObjectiveTTP(instancePath, instanceName[j]));
        }
        int thresholdComputingTimeInMilliseconds = 600;
        int populationSize = 100;
        int INDEPENDENT_RUNS = 1;
        List<TaggedAlgorithm<List<PermBinSolution>>> algorithmList = configureAlgorithmList(
            problemList, INDEPENDENT_RUNS, populationSize, thresholdComputingTimeInMilliseconds);
        Experiment<PermBinSolution, List<PermBinSolution>> experiment =
            new ExperimentBuilder<PermBinSolution, List<PermBinSolution>>("MOTTPStudy")
                .setAlgorithmList(algorithmList)
                .setProblemList(problemList)
                .setExperimentBaseDirectory(experimentBaseDirectory)
                .setOutputParetoFrontFileName("FUN")
                .setOutputParetoSetFileName("VAR")
                .setIndicatorList(Arrays.asList(
                    new Spread<PermBinSolution>(),
                    new PISAHypervolume<PermBinSolution>()))
                .setIndependentRuns(INDEPENDENT_RUNS)
                .setNumberOfCores(8)
                .build();

        new ExecuteAlgorithms<>(experiment).run();

        //printFinalSolutionSet(population);
        //writeToFile(population,instanceName);

    }

    static List<TaggedAlgorithm<List<PermBinSolution>>> configureAlgorithmList(
        List<Problem<PermBinSolution>> problemList, int independentRuns, int popSize, int timeLim) {
        List<TaggedAlgorithm<List<PermBinSolution>>> algorithms = new ArrayList<>();

        //crossover
        CrossoverOperator<PermBinSolution> crossover = new NullCrossover<PermBinSolution>();

        //Mutation operator
        TTPMutation mutation = new TTPMutation();
        //mutation.setMuBitflipRate(1.0);

        //selection operator
        SelectionOperator<List<PermBinSolution>, PermBinSolution> selection;
        selection = new BinaryTournamentSelection<PermBinSolution>(
            new RankingAndCrowdingDistanceComparator<PermBinSolution>());

        for (int run = 0; run < independentRuns; run++) {

            for (int i = 0; i < problemList.size(); i++) {
                Algorithm<List<PermBinSolution>> algorithm = new NSGA2Time<PermBinSolution>(
                    problemList.get(i),
                    popSize,
                    timeLim,
                    crossover,
                    mutation,
                    selection);
                algorithms.add(new TaggedAlgorithm<List<PermBinSolution>>(algorithm, "NSGAII", problemList.get(i), run));
            }

        }
        return algorithms;
    }


    public static void printFinalSolutionSet(List<PermBinSolution> population) {
        Deb.echo("Pareto Solutions");
        for (int i = 0; i < population.size(); i++) {
            Deb.echo(population.get(i).getObjective(0) + "  " + population.get(i).getObjective(1));
        }
    }

    public static void writeToFile(List<PermBinSolution> population, String instanceName) {
        final String dir = System.getProperty("user.dir");
        //File f = new File(dir + "/results");
        //f.mkdir();
        File file = new File(dir + "/results/" + instanceName + ".txt");

        // if file doesnt exists, then create it
        try {
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            for (int i = 0; i < population.size(); i++) {
                bw.write(population.get(i).getObjective(0) + "  " + population.get(i).getObjective(1) + "  " + population.get(i).getOb() + " " + population.get(i).getWend() + "  " + population.get(i).totalWeightPicked() + "\n");
            }
            bw.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }

}
