# README #

## How do I get set up? ##

### Set up from scratch ###

```
#!bash

git clone https://USERNAME@bitbucket.org/shelvin01/mottp.git
```


### Download commits ###

```
#!bash

git pull origin master
```

### Upload commits ###

```
#!bash

git push origin master
```

### Dependencies ###

Dependencies can be manages in the "build.gradle" file

### Database configuration ###

Database folder can be set up in the "config.properties" file

## Quick fixes ##

### Fix the java files not running problem ###

Go to File -> project structure -> modules -> then add the src/ folder to Source (choose the src/ folder, then mark as Source)

### Hard reset of last commit ###

1. Listing commits:

```
#!bash
git reflog
```

2. Go back:

```
#!bash
git reset --hard <HASH>
```